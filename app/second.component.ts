import {Component} from "@angular/core";
import {RouteParams } from "@angular/router-deprecated";
@Component({
  selector: "secondcomponent",
  template: `
    <h2>this is the second component</h2>
    <p>no time we have to learn it fast to get it done with {{id}}</p>
  `
  })

export class SecondComponent {  
  id;
  constructor(private _route: RouteParams){
    this.id = this._route.get('id');
    }
  }
