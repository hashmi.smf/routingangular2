"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_deprecated_1 = require("@angular/router-deprecated");
var first_component_1 = require("./first.component");
var second_component_1 = require("./second.component");
var AppComponent = (function () {
    function AppComponent() {
        this.title = "Hashmi";
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: "myapp",
            template: "\n    <h2>{{title}}</h2>\n    <h3>welcome to angular2 application</h3>\n    <div>\n      <ul>\n        <li>\n          <a [routerLink]= \"['Route1']\">First Route </a>\n        </li>\n        <li>\n          <a [routerLink]= \"['Route2', {id: 1}]\"> Second Route </a>\n        </li>\n      </ul>\n    </div>\n    <router-outlet></router-outlet>\n  ",
            directives: [router_deprecated_1.ROUTER_DIRECTIVES]
        }),
        router_deprecated_1.RouteConfig([
            { path: '/route1', name: 'Route1', component: first_component_1.FirstComponent, useAsDefault: true },
            { path: '/route2/:id', name: 'Route2', component: second_component_1.SecondComponent }
        ]), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map