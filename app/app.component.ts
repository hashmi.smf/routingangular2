import {Component} from "@angular/core";
import { RouteConfig, ROUTER_DIRECTIVES } from "@angular/router-deprecated";
import { FirstComponent } from "./first.component";
import { SecondComponent } from "./second.component";
@Component({
  selector: "myapp",
  template: `
    <h2>{{title}}</h2>
    <h3>welcome to angular2 application</h3>
    <div>
      <ul>
        <li>
          <a [routerLink]= "['Route1']">First Route </a>
        </li>
        <li>
          <a [routerLink]= "['Route2', {id: 1}]"> Second Route </a>
        </li>
      </ul>
    </div>
    <router-outlet></router-outlet>
  `,
  directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
  {path: '/route1', name: 'Route1', component: FirstComponent, useAsDefault: true},
  {path: '/route2/:id', name: 'Route2', component: SecondComponent}
])
export class AppComponent { 
  title = "Hashmi";
  }
