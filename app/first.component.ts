import {Component} from "@angular/core";

@Component({
  selector: "firstcomponent",
  template: `
    <h2>this is the first component</h2>
    <p>no time we have to learn it fast to get it done</p>
  `
  })

export class FirstComponent {  }
